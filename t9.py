#!/usr/bin/env python3

# Usage: ./t9.py [four digit number] 

import sys

T9 = ([],
[],
['A', 'B', 'C'], # 2 @ numpad
['D', 'E', 'F'],
['G', 'H', 'I'],
['J', 'K', 'L'],
['M', 'N', 'O'],
['P', 'Q', 'R', 'S'],
['T', 'U', 'V'],
['W', 'X', 'Y', 'Z'])

number = list(sys.argv[1])

for i in T9[number[0]]:
        for j in T9[number[1]]:
                for k in T9[number[2]]:
                        for l in T9[number[3]]:
                                print(i + j + k + l)
